=============================
Gregory: Date Spans in Python
=============================

"Date ranges and sequences for Neanderthals"

Gregory is a library for working with ranges of dates and sequences of dates in an
intuitive way.

Installation
============

    pip install gregory

Date ranges
===========

Initializing a date range
-------------------------

A date range is defined by a start date and an end date.::

    >>> DateSpan(date(2020, 1, 1), date(2020, 4, 12))
    <DateSpan 2020-01-01::2020-04-12>

The start and end date may be the same but the end date cannot come before the start.

    >>> DateSpan(date(2020, 1, 1), date(2010, 4, 12))
    Traceback (most recent call last):
        ...
    ValueError: end date 2010-04-12 cannot precede start date 2020-01-01

You must provide both start and end.::

    >>> DateSpan(date(2010, 1, 1), None)
    Traceback (most recent call last):
        ...
    ValueError: cannot initialize a DateSpan with NoneType values

Containers
----------

A date range has a length like any other container.::

    >>> len(DateSpan(date(2020, 1, 1), date(2020, 1, 12)))
    12
    >>> len(DateSpan(date(2020, 1, 1), date(2020, 1, 1)))
    1

This means it has a boolean value::

    >>> bool(DateSpan(date(2020, 1, 1), date(2020, 1, 12)))
    True

A special case, represented by the `EmptySpan` class, is used for for empty spans::

    >>> len(EmptySpan())
    0

Naturally, it always evaluates to `False`.

    >>> bool(EmptySpan())
    False

And an `EmptySpan` is still a `DateSpan`::

    >>> isinstance(EmptySpan(), DateSpan)
    True

As containers `DateSpans` can be iterated over::

    >>> list(DateSpan(date(2020, 1, 1), date(2020, 1, 3)))
    [datetime.date(2020, 1, 1), datetime.date(2020, 1, 2), datetime.date(2020, 1, 3)]

Even empty containers::

    >>> list(EmptySpan())
    []

Set-like
--------

Date ranges are *set like* in many ways, although this is a limited analogy.

Test a date for membership in a `DateSpan`::

    >>> date(2020, 1, 14) in DateSpan(date(2020, 1, 1), date(2020, 3, 17))
    True

    >>> date(2020, 1, 18) in DateSpan(date(2020, 1, 1), date(2020, 3, 17))
    False

    >>> date(2020, 1, 14) in EmptySpan()
    False

`DateSpans` can be compared and here the set comparison is most apt::

    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) == DateSpan(date(2020, 1, 1), date(2020, 2, 28))
    True
    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) == DateSpan(date(2019, 12, 31), date(2020, 2, 27))
    False
    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) == EmptySpan()
    False
    >>> EmptySpan() == EmptySpan()
    True

Less than/greater than comparisons test for subset/superset relationships.::

    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) >= DateSpan(date(2020, 1, 1), date(2020, 2, 28))
    True
    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) >= DateSpan(date(2020, 1, 21), date(2020, 2, 2))
    True
    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) > DateSpan(date(2020, 1, 1), date(2020, 2, 28))
    False
    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) > DateSpan(date(2019, 12, 31), date(2020, 1, 2))
    False

The intersection of two `DateSpans` may be useful to know::

    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)) & DateSpan(date(2020, 2, 1), date(2020, 7, 31))
    <DateSpan 2020-02-01::2020-02-28>

    >>> DateSpan(date(2010, 1, 1), date(2010, 2, 28)) & DateSpan(date(2020, 2, 1), date(2020, 7, 31))
    <EmptySpan>

Disjointedness is a property of sets that `DateSpans` can be tested for::

    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)).isdisjoint(DateSpan(2020, 2, 20), date(2020, 7, 31)))
    False
    >>> DateSpan(date(2020, 1, 1), date(2020, 2, 28)).isdisjoint(DateSpan(2028, 1, 1), date(2020, 2, 28)))
    True

Here the analogy ends. `DateSpans` are directional and their members are continuous (not in the
mathematical sense); a `DateSpan` must be composed on one stream of continguous dates. This obviates
set symmetric difference, for example. Union *could* be supported, but it would need to work either
like `DateSpan` addition (see below) or return an `EmptySpan` or raise an exception for disjoint
`DateSpans`.

Sequences
---------

`DateSpans` can be indexed much like any other sequence::

    >>> span = DateSpan(date(2020, 1, 1), date(2020, 1, 10))
    >>> span[0]
    datetime.date(2020, 1, 1)
    >>> span[20]
    Traceback (most recent call last):
        ...
    IndexError: index out of range

Negative indexes are allowed::

    >>> span[-1]
    datetime.date(2020, 1, 10)

This includes slicing::

    >>> span[:1]
    <DateSpan 2020-01-01::2020-01-01>

    >>> span[:3]
    <DateSpan 2020-01-01::2020-01-03>

    >>> span[2:5]
    <DateSpan 2020-01-03::2020-01-05>

But not steps! The date sequence would be discontinuous then.::

    >>> span[2:5:2]
    Traceback (most recent call last):
        ...
    IndexError: cannot take a stepped slice of a DateSpan

Stepping by 1 is the only explicitly allowed step.

DateSpan Arithmetic
-------------------

A `DateSpan` could be defined by a start date and a length in days just as well
a start date and end and has a direction along the axis of time (currently this
library only supports forward ranges).

Addition
~~~~~~~~

Adding two `DateSpans` result in a new `DateSpan` that includes the ranges in both
and because spans cannot be discontinuous *all dates in between* if they are not
adjacent.::

    >>> DateSpan(date(2020, 1, 1), date(2010, 1, 20)) + DateSpan(date(2020, 1, 10), date(2020, 4, 5))
    <DateSpan 2020-01-01::2020-04-05>

    >>> DateSpan(date(2020, 1, 1), date(2010, 1, 20)) + DateSpan(date(2028, 9, 10), date(2030, 4, 5))
    <DateSpan 2020-01-01::2030-04-05>

You can also add `relativedeltas` to `DateSpans`. This always works from the *end out*::

    >>> DateSpan(date(2020, 1, 1), date(2010, 1, 20)) + relativedelta(days=10)
    <DateSpan 2020-01-01::2020-01-30>

Subtraction
~~~~~~~~~~~

Subtraction is for returning a `DateSpan` *between* two other `DateSpans`.::

    >>> DateSpan(date(2020, 1, 1), date(2010, 1, 20)) - DateSpan(date(2028, 9, 10), date(2030, 4, 5))
    <DateSpan 2020-01-21::2028-09-09>

The difference between two non-adjacent spans is the same no matter which is 'subtracted'
from which::

    >>> DateSpan(date(2028, 9, 10), date(2030, 4, 5)) - DateSpan(date(2020, 1, 1), date(2010, 1, 20))
    <DateSpan 2020-01-21::2028-09-09>

For overlapping or adjacent spans the result in an `EmptySpan`.::

    >>> DateSpan(date(2020, 1, 1), date(2010, 1, 20)) - DateSpan(date(2020, 1, 10), date(2030, 4, 5))
    <EmptySpan>

Shifting
~~~~~~~~

The bit-shift operators are undefined for dates and sequences. These are used as *date shift* operators
for `DateSpans`.

Date vectors
============

TODO

A `DateVector` is very similar to a `DateSpan` but it is defined by a date and a length of time.
The length of time may be augmented by rules that override simple calendar day length.

Date sequence
=============

A date sequence has a start and rules for when each component date is yielded, including an optional
and date::

    >>> import gregory.rules as gr
    >>> seq = DateSequence(date(2020, 1, 15), gr.FirstOfMonth)
    >>> seq
