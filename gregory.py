# -*- coding: utf-8 -*-

__author__ = 'Ben Lopatin'
__email__ = 'ben@benlopatin.com'
__version__ = '0.1.0'


from datetime import date
from typing import Dict
from typing import Generator
from typing import List
from typing import Union

from dateutil.relativedelta import relativedelta


class EmptySpan:
    """
    The null DateSpan

    This is the identity for DateSpans
    """
    __slots__ = ['start', 'end']

    def __init__(self):
        self.start = None
        self.end = None

    def __repr__(self) -> str:
        return "EmptySpan"

    def __len__(self) -> int:
        return 0

    def __bool__(self) -> bool:
        return False

    def __eq__(self, other) -> bool:
        if isinstance(other, EmptySpan):
            return True
        return False

    def __contains__(self, item) -> bool:
        return False

    def __getitem__(self, item):
        # Treat int and slices differently
        return self

    def __iter__(self):
        raise StopIteration

    def __next__(self):
        raise StopIteration

    def __add__(self, other):
        return other

    def __mul__(self, other):
        return self

    def __truediv__(self, other):
        return self



class DateSpan(EmptySpan):
    """
    A span of calendar days

    A date span is basically a calendar vector
    """

    def __init__(self, start: Union[date, None], end: Union[date, None]) -> None:
        """Initializes the instance values

        Performs some value checks to ensure that the end date does not come before
        the start date and to ensure that `None` values are not provided.

        `None` values *are* allowed, however they can only be assigned when created
        by the `EmptySpan` subclass.

        >>> DateSpan(date(2020, 1, 1), date(2020, 4, 12))
        <DateSpan 2020-01-01::2020-04-12>

        >>> DateSpan(date(2020, 1, 1), date(2010, 4, 12))
        Traceback (most recent call last):
            ...
        ValueError: end date 2010-04-12 cannot precede start date 2020-01-01

        >>> DateSpan(date(2010, 1, 1), None)
        Traceback (most recent call last):
            ...
        ValueError: cannot initialize a DateSpan with NoneType values

        """
        if start is not None and end is not None and start > end:
            raise ValueError(f"end date {end} cannot precede start date {start}")
        self.start = start
        self.end = end
        if (start is None or end is None) and self.__class__.__name__ != 'EmptySpan':
            raise ValueError("cannot initialize a DateSpan with NoneType values")
        if self.start == self.end:
            self = EmptySpan()

    def __repr__(self) -> str:
        """Returns the instance specific string representation

        >>> DateSpan(date(2020, 8, 12), date(2020, 9, 22))
        <DateSpan 2020-08-12::2020-09-22>

        """
        return f"<DateSpan {self.start}::{self.end}>"

    def __bool__(self) -> bool:
        """In case someone tries to make a 0-len

        >>> bool(DateSpan(date(2020, 8, 12), date(2020, 9, 22)))
        True

        """
        return bool(self.start and self.end)

    # Container-like operations

    def __len__(self) -> int:
        """Returns the number of days in the span

        >>> len(DateSpan(date(2020, 1, 1), date(2020, 1, 31)))
        31

        >>> len(DateSpan(date(2020, 1, 1), date(2020, 1, 1)))
        1

        """
        return (self.end - self.start).days + 1

    def __contains__(self, item: date) -> bool:
        """Checks for an inclusive 'in' relationship

        >>> date(2020, 1, 1) in DateSpan(date(2020, 1, 1), date(2020, 1, 31))
        True

        >>> date(2020, 1, 31) in DateSpan(date(2020, 1, 1), date(2020, 1, 31))
        True

        >>> date(2020, 1, 15) in DateSpan(date(2020, 1, 1), date(2020, 1, 31))
        True

        >>> date(2020, 2, 1) in DateSpan(date(2020, 1, 1), date(2020, 1, 31))
        False

        """
        if isinstance(item, date):
            return self.start <= item <= self.end
        raise NotImplementedError(f"No membership test defined for {item.__class__}")

    def __getitem__(self, key: Union[int, slice]) -> 'DateSpan':
        """Get an item by integer index from the range

        >>> d = DateSpan(date(2020, 1, 1), date(2020, 1, 10))

        >>> d[0]
        datetime.date(2020, 1, 1)

        >>> d[4]
        datetime.date(2020, 1, 5)

        >>> d[-1]
        datetime.date(2020, 1, 10)

        >>> d[12]
        Traceback (most recent call last):
            ...
        IndexError: Index key 12 is outside the bounds of <DateSpan 2020-01-01::2020-01-10>

        """

        if isinstance(key, int):
            if abs(key) >= len(self):
                raise IndexError(f"Index key {key} is outside the bounds of {self}")
            if key >= 0:
                result = self.start + relativedelta(days=key)
            else:
                result = self.end + relativedelta(days=key + 1)
            if not self.start <= result <= self.end:
                raise IndexError(f"{result} is not between start {self.start} and end {self.end}")
        elif isinstance(key, slice):
            start, stop, step = key.start, key.stop, key.step
            if step not in [None, 1]:
                raise IndexError("cannot take a stepped slice of a DateSpan")

            if not start:
                if stop >= len(self):
                    return DateSpan(self.start, self.end)
                elif stop > 0:
                    return DateSpan(self.start, self.start + relativedelta(days=stop - 1))
                elif stop < 0 and abs(stop) < len(self):
                    return DateSpan(self.start, self.start + relativedelta(days=len(self) + stop - 1))
                return EmptySpan()

            else:
                if start >= stop:
                    return EmptySpan()
                elif stop < 0 and abs(stop) < len(self):
                    return DateSpan(self.start, self.start + relativedelta(days=len(self) + stop - 1))
                elif start >= 0 and stop >= len(self):
                    try:
                        return DateSpan(self.start + relativedelta(days=start), self.end)
                    except ValueError:
                        return EmptySpan()
                return EmptySpan()
        else:
            raise TypeError("Unindexable type")
        return result

    def __iter__(self) -> Generator[None, 'DateSpan', None]:
        """Iterates through all the dates in the DateSpan

        >>> list(DateSpan(date(2020, 1, 1), date(2020, 1, 4)))
        [datetime.date(2020, 1, 1), datetime.date(2020, 1, 2), datetime.date(2020, 1, 3), datetime.date(2020, 1, 4)]

        >>> list(DateSpan(date(2020, 1, 1), date(2020, 1, 1)))
        [datetime.date(2020, 1, 1)]

        """
        for i in range(len(self)):
            yield self.start + relativedelta(days=i)

    def __next__(self) -> 'DateSpan':
        """Returns a DateSpan of the same length starting immediately after this one

        >>> first = DateSpan(date(2020, 1, 1), date(2020, 1, 4))
        >>> second = next(first)
        >>> second
        <DateSpan 2020-01-05::2020-01-08>
        >>> next(second)
        <DateSpan 2020-01-09::2020-01-12>

        """
        return DateSpan(
            self.end + relativedelta(days=1),
            self.end + relativedelta(days=len(self)),
        )

    # Set-like operations

    def __eq__(self, other) -> bool:
        """Returns true for perfect overlap

        >>> DateSpan(date(2020, 4, 13), date(2023, 2, 17)) == DateSpan(date(2020, 4, 13), date(2023, 2, 17))
        True
        >>> DateSpan(date(2020, 4, 15), date(2023, 2, 17)) == DateSpan(date(2020, 4, 13), date(2023, 2, 17))
        False
        >>> DateSpan(date(2020, 4, 15), date(2023, 2, 17)) == EmptySpan()
        False

        """
        if self.start == self.end and isinstance(other, EmptySpan):
            return True
        return self.start == other.start and self.end == other.end

    def __ge__(self, other) -> bool:
        """Returns True if every element of other is in self

        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) >= DateSpan(date(2020, 1, 1), date(2020, 3, 31))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) >= DateSpan(date(2020, 1, 1), date(2020, 5, 31))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 1)) >= DateSpan(date(2020, 1, 1), date(2020, 1, 31))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 1)) >= DateSpan(date(2020, 2, 1), date(2020, 2, 28))
        True

        """
        return self.start <= other.start and self.end >= other.end

    def __gt__(self, other) -> bool:
        """Returns True if self is superset of other

        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) > DateSpan(date(2020, 1, 1), date(2020, 3, 31))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) > DateSpan(date(2020, 1, 1), date(2020, 5, 31))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 1)) > DateSpan(date(2020, 1, 1), date(2020, 1, 31))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 1)) > DateSpan(date(2020, 2, 1), date(2020, 2, 28))
        True

        """
        return self.start <= other.start and self.end >= other.end and not self == other

    def __le__(self, other) -> bool:
        """Returns True if every element of self is in other

        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) <= DateSpan(date(2020, 1, 1), date(2020, 3, 31))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) <= DateSpan(date(2020, 1, 1), date(2020, 5, 31))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 1, 31)) <= DateSpan(date(2020, 1, 1), date(2020, 3, 1))
        True
        >>> DateSpan(date(2020, 2, 1), date(2020, 3, 2)) <= DateSpan(date(2020, 1, 1), date(2020, 3, 1))
        False

        """
        return self.start >= other.start and self.end <= other.end

    def __lt__(self, other) -> bool:
        """Returns True if self is subset of other

        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) < DateSpan(date(2020, 1, 1), date(2020, 3, 31))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)) < DateSpan(date(2020, 1, 1), date(2020, 5, 31))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 1, 31)) < DateSpan(date(2020, 1, 1), date(2020, 3, 1))
        True
        >>> DateSpan(date(2020, 2, 1), date(2020, 3, 2)) < DateSpan(date(2020, 1, 1), date(2020, 3, 1))
        False

        """
        return self.start >= other.start and self.end <= other.end and not self == other

    def isdisjoint(self, other: 'DateSpan') -> bool:
        """Returns True if spans have no overlap

        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isdisjoint(DateSpan(date(2020, 1, 1), date(2020, 3, 31)))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isdisjoint(DateSpan(date(2020, 3, 31), date(2022, 3, 31)))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isdisjoint(DateSpan(date(2020, 4, 1), date(2022, 3, 31)))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isdisjoint(EmptySpan())
        True

        """
        if not self or not other:
            return True
        return self.start > other.end or other.start > self.end

    def isadjacent(self, other: 'DateSpan') ->bool:
        """Tests for ther the spans start/end right before one another

        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isadjacent(DateSpan(date(2020, 1, 1), date(2020, 3, 31)))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isadjacent(DateSpan(date(2020, 3, 31), date(2022, 3, 31)))
        False
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isadjacent(DateSpan(date(2020, 4, 1), date(2022, 3, 31)))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isadjacent(DateSpan(date(2019, 4, 1), date(2019, 12, 31)))
        True
        >>> DateSpan(date(2020, 1, 1), date(2020, 3, 31)).isadjacent(DateSpan(date(2019, 4, 1), date(2019, 12, 30)))
        False

        """
        if self == other:
            return False
        if self.start > other.end and other.end + relativedelta(days=1) == self.start:
            return True
        if self.end < other.start and self.end + relativedelta(days=1) == other.start:
            return True
        return False

    # Arithmetic!?

    def __add__(self, other) -> 'DateSpan':
        """Add two DateSpans"""
        return DateSpan(
            min(self.start, other.start),
            max(self.end, other.end),
        )

    def __sub__(self, other) -> 'DateSpan':
        """Subtract two DateSpans

        """
        if any([
                self.end == other.end,
                self.end == other.start,
                self.start == other.start,
                self.start == other.end,
        ]):
            return EmptySpan()
        elif self.start <= other.start and self.end >= other.start - relativedelta(days=1):
            return EmptySpan()
        elif self.start >= other.start and self.start <= other.end + relativedelta(days=1):
            return EmptySpan()
        elif self.end < other.start:
            return DateSpan(
                self.end + relativedelta(days=1),
                other.start - relativedelta(days=1)
            )
        else:
            return DateSpan(
                other.end + relativedelta(days=1),
                self.start - relativedelta(days=1)
            )

    def __mul__(self, other: int) -> Union['DateSpan', List['DateSpan']]:
        if other < 0:
            raise ValueError("Sorry, spans cannot yet be negative")
        if other == 0:
            return DateSpan(start=self.start, end=self.start)
        return [self] + [next(self) for _ in range(other - 1)]

    # Shifting operations
    # Here we're definitely hijacking operators but since dates don't define behavior
    # for bitwise shifting, this is actually way less weird as it't not *changing* some
    # expected behavior.

    def __lshift__(self, other: relativedelta) -> 'DateSpan':
        """Left shift by `other` time difference

        >>> DateSpan(date(2020, 1, 1), date(2020, 1, 10)) << relativedelta(days=3)
        <DateSpan 2019-12-29::2020-01-07>

        """
        return DateSpan(
            start=self.start - other,
            end=self.end - other,
        )

    def __rshift__(self, other: relativedelta) -> 'DateSpan':
        """Right shift by `other` time difference

        >>> DateSpan(date(2020, 1, 1), date(2020, 1, 10)) >> relativedelta(days=3)
        <DateSpan 2020-01-04::2020-01-13>

        """
        return DateSpan(
            start=self.start + other,
            end=self.end + other,
        )


class Month(DateSpan):
    """
    Represents an arbitrary month.

    Unlike DateSpan this follows calendar month boundaries
    """

    next_options = {'months': 1}

    def __init__(self, start: date, cal_date: date=None) -> None:
        super().__init__(start, self.last_day(start, self.next_options))
        self.cal_date = cal_date or start.day

    def __repr__(self) -> str:
        return f"<Month {self.start} - {self.end}>"

    def __next__(self):
        next_start = self.start + relativedelta(**self.next_options)
        while next_start.day < self.cal_date:
            print(next_start, self.cal_date)
            try:
                next_start = next_start.replace(day=next_start.day + 1)
            except ValueError:
                return self.__class__(next_start, cal_date=self.cal_date)

        return self.__class__(next_start, cal_date=self.cal_date)

    @staticmethod
    def last_day(start: date, next_options: Dict) -> date:
        """Static method so it can be referenced before initialization"""
        return start + relativedelta(**next_options) - relativedelta(days=1)
