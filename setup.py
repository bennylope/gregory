#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.rst') as readme_file:
    readme = readme_file.read()

#with open('HISTORY.rst') as history_file:
history = ""

requirements = [
    'python-dateutil',
    'arrow',
]

test_requirements = [
    'pytest',
    'hypothesis',
    # TODO: put package test requirements here
]

setup(
    name='gregory',
    version='0.1.0',
    description="Dates for Neanderthals",
    long_description=readme + '\n\n' + history,
    author="Ben Lopatin",
    author_email='ben@benlopatin.com',
    url='https://github.com/bennylope/gregory',
    #packages=[
    #    'gregory',
    #],
    #package_dir={'gregory':
    #             'gregory'},
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='gregory',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
